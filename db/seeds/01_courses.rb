Course.where(name: 'Bachelor of Science in Computer Engineering', abbreviation: "BSCpE").first_or_create
Course.where(name: 'Bachelor of Science in Civil Engineering', abbreviation: "BSCE").first_or_create
Course.where(name: 'Bachelor of Science in Accountancy', abbreviation: "BSAC").first_or_create
Course.where(name: 'Bachelor of Science in Business Administration', abbreviation: "BSBA").first_or_create
Course.where(name: 'Bachelor of Science in Industrial Technology', abbreviation: "BSIT").first_or_create
Course.where(name: 'Bachelor of Technical Teacher Education', abbreviation: "BTTE").first_or_create